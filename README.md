A collection of lowercase English dictionary words sorted according to lexicographic order which can serve as a source for data generation of various tests.

Use _compiled_words.txt which contains the collection of 110262 English dictionary words at this moment. Other (.txt) files contains words with their meanings, can be used as a mini dictionary too.
